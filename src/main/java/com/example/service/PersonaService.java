package com.example.service;

import java.util.List;

import com.example.bean.Persona;

public interface PersonaService {
	
	List<Persona> getPersonas();
	void agregarPersona(Persona persona);

}
