package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.bean.Persona;

@Service
public class PersonaServiceImpl implements PersonaService{

private List<Persona> personas;

	
	@Override
	public void agregarPersona(Persona persona) {
		this.getPersonas().add(persona);
	}
	
	@Override
	public List<Persona> getPersonas() {
		if (personas == null)
			personas = new ArrayList<>();
		return personas;
	}
	


}
